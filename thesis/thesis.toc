\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\contentsline {chapter}{Acknowledgements}{v}{section*.2}
\contentsline {chapter}{Abstract}{vii}{section*.3}
\contentsline {chapter}{Outline of the Thesis}{xi}{section*.5}
\contentsline {part}{\numberline {I}Introduction and Theory}{1}{part.1}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Big Data and Hadoop}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Project}{3}{section.1.2}
\contentsline {chapter}{\numberline {2}State Of The Art}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Big Data}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}MapReduce and Hadoop}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Related Projects}{8}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Alternative Implementations}{8}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Alternative Paradigms}{8}{section.2.3}
\contentsline {chapter}{\numberline {3}Theory}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}The MapReduce Paradigm}{11}{section.3.1}
\contentsline {section}{\numberline {3.2}Hadoop Implementation}{14}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Hadoop 1}{15}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Hadoop 2}{17}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Additional Language Support}{19}{subsection.3.2.3}
\contentsline {part}{\numberline {II}The Application}{23}{part.2}
\contentsline {chapter}{\numberline {4}Overview}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}Requirements}{25}{section.4.1}
\contentsline {section}{\numberline {4.2}Technologies Adopted}{25}{section.4.2}
\contentsline {chapter}{\numberline {5}Implementation}{29}{chapter.5}
\contentsline {section}{\numberline {5.1}Architecture}{29}{section.5.1}
\contentsline {section}{\numberline {5.2}Configuration}{29}{section.5.2}
\contentsline {section}{\numberline {5.3}Mathematical Toolbox}{31}{section.5.3}
\contentsline {section}{\numberline {5.4}Optimizations}{43}{section.5.4}
\contentsline {chapter}{\numberline {6}Infrastructure}{45}{chapter.6}
\contentsline {section}{\numberline {6.1}Hadoop}{45}{section.6.1}
\contentsline {section}{\numberline {6.2}Redis}{46}{section.6.2}
\contentsline {section}{\numberline {6.3}Configuration}{46}{section.6.3}
\contentsline {chapter}{\numberline {7}Profiling Results}{49}{chapter.7}
\contentsline {section}{\numberline {7.1}Hadoop}{49}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Block Size}{50}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Replication Factor}{51}{subsection.7.1.2}
\contentsline {subsection}{\numberline {7.1.3}Number Of Mappers}{51}{subsection.7.1.3}
\contentsline {subsection}{\numberline {7.1.4}Number Of Reducers}{52}{subsection.7.1.4}
\contentsline {subsection}{\numberline {7.1.5}Optimal Configuration}{53}{subsection.7.1.5}
\contentsline {section}{\numberline {7.2}Comparison With Single Core Implementation}{54}{section.7.2}
\contentsline {section}{\numberline {7.3}Comparison With Multiple Core Implementation}{57}{section.7.3}
\contentsline {section}{\numberline {7.4}Additional Experiments}{58}{section.7.4}
\contentsline {part}{\numberline {III}Outlook}{61}{part.3}
\contentsline {chapter}{\numberline {8}Conclusion}{63}{chapter.8}
\contentsline {chapter}{\numberline {9}Future Work}{65}{chapter.9}
\contentsline {chapter}{Bibliography}{67}{chapter*.60}
