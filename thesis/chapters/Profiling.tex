\chapter{Profiling Results} \label{chapter:Profiling}
After the implementation, the performance of the application under different conditions was tested.

The profiling was performed to understand what is the best configuration that provides the maximum performance for every test case. 

Firstly, the results of several Hadoop configurations will be presented, and afterwards, these will be compared to single and multiple core implementations in order to understand if Hadoop offers a more efficient solution for the dataset usually handled by the company and if it provides a benefit over the technologies presently used.

\section{Hadoop} \label{sec:profiling-hadoop}
During the profiling, all the combinations of the following configurations were tested:

\begin{enumerate}
  \item {\bfseries Block Size:} HDFS automatically created for every file some meta-data which specifies on which machine the data are actually stored, how many times it is replicated, etc. If the files are large, they are split into blocks. Every block owns its own meta-data, so that it can be treated independently from the whole file. 
  This is particularly true during the mapping phase. By default, Hadoop assigns a mapper to every block.
  The standard size of the blocks is 64MB. However, it is possible to change it to adapt the application to the users' needs. For the experiments, the considered block sizes were 32MB, 64MB and 128MB.
  \item {\bfseries Replication Factor:} The replication factor specifies how many times a block is spread across the infrastructure. A high replication factor should be an advantage for the methods that contain computing intensive operations within the mappers because the data is already scattered and does not need to be communicated at run time to idle nodes. On the other hand, that could also present a drawback where the reducers produce large results because these have to be copied many times across the infrastructure. % It is also possible to manually set high replication factors only for certain files, while the results are afterwards distributed only few times.
  \item {\bfseries Number of Mappers:} Hadoop allows to suggest a the number of mappers to use. The framework will, however, use internal optimizations to select how many mappers will be run. A high number can allow more parallelization on large infrastructures but can be a redundant burden if the computing nodes are fewer than the mappers. Several mappers might be run on locally close data, creating unnecessary waiting times. Additional overhead is caused by the sorting and the communication to the reducers.
  \item {\bfseries Number of Reducers:} The users can request an exact number of reducers that will be executed at runtime. The optimal quantity of reducers to be run depends highly on the problem. For certain operations, a high number presents a communication bottleneck that slows down the whole execution. While in other contexts, the computation can benefit thanks to the automatic load distribution. If lots of reduces are adopted, failures also become less critical because the ongoing work can be quickly recovered by other task trackers.
  % \item {\bfseries Dataset:} Different datasets can be taken into account to see how Hadoop scales across different input sizes. For the experiments, data between 100MB and 1GB are employed.
\end{enumerate}

Every experiment was run three times to provide a safer measurement that should be less prone to seldom occurring issues in the infrastructure like OS errors, network lags, overloading of resources by other applications, etc.

The dataset used for the experiments was a set of 38480 time series stored in a single file of approximately 1.5GB.

In order to be able to perform the tests, the maximum amount of memory usable by Hadoop had to be shifted to 1GB, instead of the default 512MB. Without this change the framework was crashing while running some tests.

\subsection{Block Size}
The comparison of the block sizes was performed by choosing the best combination of mappers and reducers for the same replication factor.

The results shown in the following table describe an general increase in performance if lower block sizes are used. This could be due to the fact that in the considered scenarios, lower block sizes contribute to a higher parallelization of the problem. In the table, the data of the correlation was scaled by a factor of 10 to fit better in the graph.

\begin{center}
  \pgfplotstabletypeset
  [
    col sep=&,
    every head row/.style={
      % as in the previous example, this patches the first row:
      before row=\hline,
      after row=\hline,
    },
    every last row/.style={after row=\hline},
    % define column-specific styles:
    columns/Size/.style={column type=|c,string type, column name=Block Size (MB)},
    columns/Tukey/.style={column type=|c,column name=Tukey (s)},
    columns/Statistical/.style={column type=|c,column name=Statistical (s)},
    columns/Autocorrelation/.style={column type=|c,column name=Autocorrelation (s)},
    columns/Correlation/.style={column type=|c|,column name=Correlation (s)},
  ]
  {plotdata/block_size_32_64_128.dat}
\end{center}

However, in the considered cases, the differences are up to 1.5 times. Probably with larger datasets, it could be possible to see higher variations.

In the graph \ref{graph:block-size}, it is possible to observe how the running times of the previous table evolve for different block sizes.
\begin{graph}
\begin{center}
  \resizebox {\columnwidth} {!} {
    \begin{tikzpicture}
      \begin{axis}[
        legend style={ at={(1.03,0.03)},anchor= south west},
%        title=Running Time,
        grid=major,
        xlabel=Blocks Size (MB),
        ylabel=Time (s)
      ]
      
        \addplot[red, mark=triangle*]
          table[col sep=&, x=Size, y=Tukey]
          {plotdata/block_size_32_64_128.dat};
        \addlegendentry{Tukey};

        \addplot[blue, mark=square*]
          table[col sep=&, x=Size, y=Statistical]
          {plotdata/block_size_32_64_128.dat};
        \addlegendentry{Statistical};

        \addplot[brown, mark=pentagon*]
          table[col sep=&, x=Size, y=Autocorrelation]
          {plotdata/block_size_32_64_128.dat};
        \addlegendentry{Autocorrelation};
        
        \addplot[black, mark=o]  
          table[col sep=&, x=Size, y=Correlation]
         {plotdata/block_size_32_64_128.dat};
        \addlegendentry{Correlation};
      
      \end{axis}
    \end{tikzpicture}%
  }
  \caption{Running time for different block sizes.}
  \label{graph:block-size}
\end{center}
\end{graph}

\FloatBarrier

\subsection{Replication Factor}
The replication factor does not produce major performance improvements or regressions of the results. Depending on the functions, there can be minor shifts of performance. 

In all the cases shown below, the results represent the average running time in seconds of several experiments run with a fixed block size of 64 MB, a variable number of Mappers and Reducers.

If the application puts more focus on the reading from disk, the execution time may gain slightly benefits from a higher replication factor. It is the case of the statistical functions when the results are written to Redis. 

\begin{center}
  \pgfplotstabletypeset[
  col sep=&,
  row sep=\\,
  every head row/.style={
    % as in the previous example, this patches the first row:
    before row=\hline,
    after row=\hline,
  },
  every last row/.style={after row=\hline},
  % define column-specific styles:
  columns/rf2/.style={column type=|c,column name=Replication Factor: 2 (s)},
  columns/rf4/.style={column type=|c,column name=Replication Factor: 4 (s)},
  columns/improvement/.style={column type=|c|,column name=Improvement $\frac{Rep Fact 2}{Rep Fact 4}$},
  ]
  {
    rf2    & rf4    & improvement\\
   153.6   & 145.6  & 1.06       \\
  }
\end{center}

On the other hand, if the functions are characterized by a quick computing part that immediately needs to be output to disk, the replication factor might degrade the performance, as it happens with the Autocorrelation test.

\begin{center}
  \pgfplotstabletypeset[
  col sep=&,
  row sep=\\,
  every head row/.style={
    % as in the previous example, this patches the first row:
    before row=\hline,
    after row=\hline,
  },
  every last row/.style={after row=\hline},
  % define column-specific styles:
  columns/rf2/.style={column type=|c,column name=Replication Factor: 2 (s)},
  columns/rf4/.style={column type=|c,column name=Replication Factor: 4 (s)},
  columns/improvement/.style={column type=|c|,column name=Improvement $\frac{Rep Fact 2}{Rep Fact 4}$},
  ]
  {
    rf2    & rf4    & improvement\\
    274.8  & 310.9  &	0.88       \\
  }
\end{center}

\FloatBarrier

\subsection{Number Of Mappers}
The default behavior is to start one mapper per block, but users can configure how many mappers to use. However, the ultimate choice will be performed by Hadoop, which will optimize it for the actual scenario.
During the tests, when 10 mappers were suggested, 18 were actually run, or when 20 mappers were proposed, 22 were launched. Hadoop decision is based on the available computing cores and amount of blocks to process. 

The best option turned out to be a number of mappers that matches the computing cores. If a low number of mappers is suggested, the resources of the system are underused. If a large number is passed, more communication in the sorting phase is involved, causing an overhead.

Anyway, the number of employed mappers does not produce major changes in the performance of the application. In graph \ref{graph:mappers}, it is possible to see the difference in performance caused by different numbers of mappers.
\begin{graph}
  \begin{center}
    \resizebox {\columnwidth} {!} {
      \begin{tikzpicture}
      \begin{axis}[
        ybar=0pt,
        %        enlargelimits=0.15,
        xmin=-0.5,
        xmax=2.5,
        ymin=0,         
        ymax=250, 
        xtick={0,1,2,...,20},
        xticklabels={Stat. Func.,Outlier Det., Autocorr },
        xticklabel style={
          anchor=base,
          yshift=-\baselineskip
        },
        ylabel=Time (s),
        legend style={ at={(1.03,0.03)},anchor= south west},
        nodes near coords=\rotatebox{90}{\scriptsize\pgfmathprintnumber\pgfplotspointmeta},
      ]
        \addplot[fill=red, postaction={ pattern=north east lines}] 
          table[y=10] {plotdata/autocorrelation_run_64_2_2_presentation.dat};
        \addlegendentry{10 Mappers};
        
        \addplot[fill=orange] 
          table[y=20] {plotdata/autocorrelation_run_64_2_2_presentation.dat};
        \addlegendentry{20 Mappers};
        
        \addplot[fill=yellow, postaction={pattern=grid}]
          table[y=40] {plotdata/autocorrelation_run_64_2_2_presentation.dat};
        \addlegendentry{40 Mappers};
        
        \addplot [fill=yellow!40!white] 
          table[y=80] {plotdata/autocorrelation_run_64_2_2_presentation.dat};
        \addlegendentry{80 Mappers};    
      \end{axis}
      \end{tikzpicture}
    }
    \caption{Running Time for different number of Mappers.}
    \label{graph:mappers}
  \end{center}
\end{graph}

For this example, the simulations were configured with a block size of 64 MB, a  replication factor of two and a fixed number of reducers.
A very similar behavior can be observed for all block sizes and replication factors.

\FloatBarrier

\subsection{Number Of Reducers}
If no configuration is explicitly set, Hadoop launches only one reducer per job. Anyway, users have the possibility to decide exactly how many reduces will be executed at runtime.

The best match for the reducers is strictly connected to the problem. 

If the problem for every reducer is quick to solve, it is better to start a number of reducers close to the actual number of computing nodes. This can be observed in graph \ref{graph:reducers}, which shows the results for the statistical functions (see Section \ref{subsec:stat-func}), where the function is run with different numbers of mappers and reducers. If too many reducers are started, a communication overhead is introduced, and no additional benefit is gained.

On the other hand, if the problem for a single reducer requires a lot of time to be solved, it is better to split the workload across the computing nodes. For the computation of the correlation matrix (see Section \ref{subsec:corrMatrImpl}), if more and more reducers are started, there is only a minimal drop in performance due to the communication overhead. Moreover, with more reducers, Hadoop is able to effectively use speculative execution (see Section \ref{par:speculative-execution}), which allows a faster execution of the overall work.

\begin{graph}
\begin{center}
  \resizebox {\columnwidth} {!} {
    \plotTimeReducers{Stat Functions}{plotdata/stat_run_64_2_2.dat}{10,20,40,80}
    \plotTimeReducers{Correlation}{plotdata/correlation_run_64_2_2.dat}{10,20,40}
  }
  \caption{Running Time for different number of Reducers.}
  \label{graph:reducers}
\end{center}
\end{graph}

For the proposed example, the simulations were run with a configured block size of 64 MB and a replication factor of two.
A very similar behavior can be observed for all block sizes and replication factors.

\FloatBarrier

\subsection{Optimal Configuration}
Once the block size and  the replication factor are set, it is always possible to find an optimal configuration for the mappers and the reduces by running several combinations. It is interesting to note that all good results are clustered around the best configuration. This can be seen if the results are plot in a table and a heat map is applied on them.

An example of this can be found in table \ref{table:heatmap}. It represents the running times for several configurations of the Tukey outlier detection (see section \ref{subsubsec:Tukey}). The first upper row represents the number of mappers suggested, and the first column the number of reducers used. The coloring is performed in order to enhance the best results: green represents the best running times while red the less optimal ones.

\begin{table}[htbp]
  \begin{center}
    \pgfplotstabletypeset[color cells={min=111,max=7300}]
    {plotdata/tukey_run_64_4_4.dat}
    \caption{Heat map of running times of Tukey detection. The first row represents the number of suggested mappers. The first column the number of used reducers.}
    \label{table:heatmap}
 \end{center}
\end{table}

\section{Comparison With Single Core Implementation}

After the application was profiled with all the configuration stated in Section \ref{sec:profiling-hadoop}, the best results are selected and compared to single core implementations, in order to understand where it is really possible to gain benefits from the Haooop framework.

The single core implementations are created using Python in combination with NumPy \cite{numpy} and SciPy \cite{scipy}. Two complementary high performance numerical libraries suited for statistics and scientific computing.

Python was chosen as a technology because it is a widely spread programming language for scientific computing, and it also allows to prototype very quickly.

The Python implementations resemble the ones proposed for MapReduce. The input is read from a file, and the results are output to a file, as well. The code for the single core implementation did not undergo a fine tuning optimization, only prototypes were used. However, they are useful to get an understanding of the overall running time. Low level, optimized implementations can surely outperform the adopted prototypes.

\subsection*{Statistical Functions}

The single core implementation yields a similar result of the fastest Hadoop run on 1 GB dataset. MapReduce already perform better, but it does not outperform.

\begin{center}
  \pythonHadoopComparison
  {
    python & hadoop & improvement\\
    367.7  & 97.77  & 3.76 \\
  }
\end{center}

The Hadoop result presented here is the outcome of the Statistical Functions run with a block size of 32 MB, a replication factor of 4, 20 mappers and 32 reducers.

\subsection*{Outliers Detection}
In the case of outlier detection, Hadoop does not outperform the single core implementation for a 1.5GB input dataset. The running times are in the same order of magnitude.

\begin{center}
  \pythonHadoopComparison
  {
    python   & hadoop     & improvement\\
    320.928  & 111.42     & 2.88 \\
  }
\end{center}

The Hadoop result presented here is the outcome of the Statistical Functions run with a block size of 64 MB, a replication factor of 4, 40 mappers and 32 reducers.

\subsection*{Autocorrelation}
Similarly to the previous cases, the autocorrelation on Hadoop shows an improvement over the single core implementation, but it does not present a huge boost in performance.
The results can be seen in the following table:

\begin{center}
  \pythonHadoopComparison
  {
    python & hadoop    & improvement\\
    554.2  & 160.831   & 3.44 \\
  }
\end{center}

This result was obtained by configuring Hadoop with a block size of 32MB, a replication factor of 2, 20 Mappers, and 16 reducers.

\subsection*{Correlation}
With Hadoop, it is possible to see an improvement from the single core implementation, which takes roughly 20 hours to complete. However, this result is very close to a Hadoop outcome if only one reducer is started.

\begin{center}
  \pythonHadoopComparison
  {
    python & hadoop    & improvement\\
    71951  & 3585      & 20.0672    \\
  }
\end{center}

The Hadoop result presented here is the outcome of the Correlation Matrix calculation run with a block size of 32 MB, a replication factor of 2, 80 mappers and 256 reducers.

\subsection*{Correlated Random Variables}
The generation of correlated random variables in Hadoop presents an advantage w.r.t. most single core implementations, as it can be seen in the tables below. 

This is a very interesting case for Riskab because it is the core of the procedure used for generating future scenarios. For this case, several implementations were compared in order to understand if Hadoop can really offer an advantage over the technologies currently adopted at the company.

The values exposed represent the running time only of the computation phase, since the loading takes a constant amount of time for all the scenarios. For the single core implementation, the loading step was very efficient if compared to Hadoop (17 seconds against 321 seconds). This is the case because for Python, the correlation matrix was stored in binary format in a single file of 6MB, while in the case of Hadoop, an entire MapReduce step had to be launched on a previously computed result stored in HDFS within a file of several GBs.

The tests with single core implementations were executed only up to $10^5$ runs of equation \ref{eq:crv}. The next results were extrapolated using linear regression.

The experiments were run with a small input dataset: the provided file was large, 75MB, and it contained 877 time series.

First a python implementation was tried. In this scenario Hadoop is able to outperform the single core execution as it is possible to see in the following table. It is interesting to notice an improvement of roughly 40 times. This is particularly meaningful because 36 computing nodes were used.

\begin{center}
  \resizebox {\columnwidth} {!} {
    \pgfplotstabletypeset
    [
      col sep=&,
  %    row sep=\\,
      every head row/.style={
        % as in the previous example, this patches the first row:
        before row=\hline,
        after row=\hline,
      },
      every last row/.style={after row=\hline},
      % define column-specific styles:
      columns/runs/.style={column type=|c,column name=Runs},
      columns/python/.style={column type=|c,column name=Python Time (s)},
      columns/pythonExtra/.style={column type=|c,column name=Python Extrap. Time (s)},
      columns/hadoop/.style={column type=|c,column name=Hadoop Time (s)},
      columns/improvementPython/.style={column type=|c|,column name=Improvement},
    ]
    {plotdata/crv_python.dat}
  }
\end{center}

The generation of the correlated random variables was also compared with other implementations that are able to better exploit the hardware capabilities of the machines.

Firstly, the function was implemented in Matlab which is a very common tool at the company. This is framework was configured to be able to take advantage of Intel's MKL. In this case, the results are an order of magnitude better than Python and closer to Hadoop, as it can be evinced from the following table.

\begin{center}
  \resizebox {\columnwidth} {!} {
    \pgfplotstabletypeset
    [
    col sep=&,
    %    row sep=\\,
    every head row/.style={
      % as in the previous example, this patches the first row:
      before row=\hline,
      after row=\hline,
    },
    every last row/.style={after row=\hline},
    % define column-specific styles:
    columns/runs/.style={column type=|c,column name=Runs},
    columns/matlab/.style={column type=|c,column name=Matlab Time (s)},
    columns/matlabExtra/.style={column type=|c,column name=Matlab Extrap. Time (s)},
    columns/hadoop/.style={column type=|c,column name=Hadoop Time (s)},
    columns/improvementMatlab/.style={column type=|c|,column name=Improvement},
    ]
    {plotdata/crv_matlab.dat}
  }
\end{center}

Afterwards, another tool often used at the company was adopted: C\# in combination with the NMath library. NMath directly calls the BLAS routines of Intel's MKL and this bundle yields to running times very close to Matlab. The results of the runs can be seen in the following table.

\begin{center}
  \resizebox {\columnwidth} {!} {
    \pgfplotstabletypeset
    [
    col sep=&,
    %    row sep=\\,
    every head row/.style={
      % as in the previous example, this patches the first row:
      before row=\hline,
      after row=\hline,
    },
    every last row/.style={after row=\hline},
    % define column-specific styles:
    columns/runs/.style={column type=|c,column name=Runs},
    columns/cSharp/.style={column type=|c,column name=NMath Time (s)},
    columns/cSharpExtra/.style={column type=|c,column name=NMath Extrap. Time (s)},
    columns/hadoop/.style={column type=|c,column name=Hadoop Time (s)},
    columns/improvementCSharp/.style={column type=|c|,column name=Improvement},
    ]
    {plotdata/crv_cSharp.dat}
  }
\end{center}

A further test with NMath was also performed. In this case, multi-threading was enabled, and 4 threads were run. For this scenario, the generation of the correlated random variables performs as good as the Hadoop implementation running on 36 cores. The results follow.

\begin{center}
  \resizebox {\columnwidth} {!} {
    \pgfplotstabletypeset
    [
    col sep=&,
    %    row sep=\\,
    every head row/.style={
      % as in the previous example, this patches the first row:
      before row=\hline,
      after row=\hline,
    },
    every last row/.style={after row=\hline},
    % define column-specific styles:
    columns/runs/.style={column type=|c,column name=Runs},
    columns/cSharpPar/.style={column type=|c,column name=NMath Par. Time (s)},
    columns/cSharpParExtra/.style={column type=|c,column name=NMath Par. Extrap. Time (s)},
    columns/hadoop/.style={column type=|c,column name=Hadoop Time (s)},
    columns/improvementCSharp/.style={column type=|c|,column name=Improvement},
    ]
    {plotdata/crv_cSharp_par.dat}
  }
\end{center}

The runtimes of the different tests are plotted in the chart \ref{graph:single-core} on the left hand side. On the right side, it is possible to see speedup provided by the Hadoop implementation.

\begin{graph}
\begin{center}
  \resizebox {\columnwidth} {!} {
    \begin{tikzpicture}
      \begin{loglogaxis}[
        legend style={ at={(1.03,0.03)},anchor=south west},
        title=Running Time,
        grid=major,
        xlabel=Num. of Runs,
        ylabel=Time (s)
      ]
      
        \addplot[blue, mark=o] 
          table[col sep=& ,x=runs, y=pythonExtra] {plotdata/crv_python.dat};
        \addlegendentry{Python Extrap.};
        
        \addplot[blue, mark=oplus*] 
          table[col sep=& ,x=runs, y=python]      {plotdata/crv_python.dat};  
        \addlegendentry{Python};
        
        \addplot[orange, mark=triangle]
          table[col sep=& ,x=runs, y=cSharpExtra] {plotdata/crv_cSharp.dat};
        \addlegendentry{NMath Extrap.};
        
        \addplot[orange, mark=triangle*]
          table[col sep=& ,x=runs, y=cSharp]      {plotdata/crv_cSharp.dat};  
        \addlegendentry{NMath};
        
        \addplot [red, mark=diamond]
          table[col sep=& ,x=runs, y=cSharpParExtra] {plotdata/crv_cSharp_par.dat};
        \addlegendentry{NMath Par. Extrap.};
        
        \addplot [red, mark=diamond*]
          table[col sep=& ,x=runs, y=cSharpPar]      {plotdata/crv_cSharp_par.dat};  
        \addlegendentry{NMath Par.};
        
        \addplot [green, mark=pentagon]
        table[col sep=& ,x=runs, y=matlabExtra]      {plotdata/crv_matlab.dat};  
        \addlegendentry{Matlab Extrap.};
        
        \addplot [green, mark=pentagon*]
        table[col sep=& ,x=runs, y=matlab]      {plotdata/crv_matlab.dat};  
        \addlegendentry{Matlab};
         
        \addplot [black, mark=square]
          table[col sep=& ,x=runs, y=hadoop]      {plotdata/crv.dat};
        \addlegendentry{Hadoop};
      
      \end{loglogaxis}
    \end{tikzpicture}%
    \begin{tikzpicture}
      \begin{loglogaxis}[
        legend style={ at={(1.03,0.03)},anchor=south west},
        title=Improvement,
        grid=major,
        xlabel=Num. of Runs,
        ylabel=Speedup
      ]
      
        \addplot[blue, mark=oplus*]
          table[col sep=& ,x=runs, y=improvementPython] {plotdata/crv_python.dat};
        \addlegendentry{$\frac{Python (s)}{Hadoop(s)}$};
        
        \addplot[orange, mark=triangle*]
          table[col sep=& ,x=runs, y=improvementCSharp] {plotdata/crv_cSharp.dat};
        \addlegendentry{$\frac{NMath (s)}{Hadoop(s)}$};
        
        \addplot[red, mark=diamond*]
         table[col sep=& ,x=runs, y=improvementCSharp] {plotdata/crv_cSharp_par.dat};
        \addlegendentry{$\frac{NMath Par. (s)}{Hadoop(s)}$}
        
        \addplot[green, mark=pentagon*]
        table[col sep=& ,x=runs, y=improvementMatlab] {plotdata/crv_matlab.dat};
        \addlegendentry{$\frac{Matlab (s)}{Hadoop(s)}$}
        
      
      \end{loglogaxis}
    \end{tikzpicture}  
  }
  \caption{Comparison with the single core implementations, single and threaded.}
  \label{graph:single-core}
\end{center}
\end{graph}

For this kind of function, Hadoop is not able to outperform single core implementations which are tuned with high performance numerical libraries. The overhead introduced by the network and the distributed file system do not allow the full exploitation of hardware resources.

Anyway, it is still important to notice that the running times are comparable to and sometimes even better than libraries used in scientific computing like SciPy and NumPy.

%From these results, it can be noted that for this kind of computation, Hadoop is able to perform an order of magnitude faster than the Python implementation. In particular for the cases that involved higher computation loads, the improvement seems to be more stable, since the communication overhead becomes minimal.

%An improvement of roughly 40 times is particularly meaningful because 36 computing nodes were employed. This shows that for this scenario, Hadoop is able to make a very efficient use of the computing infrastructure.

%The proposed comparisons also show that Hadoop is able to beat the optimized single core implementation only if large amounts of data are used.

\FloatBarrier

\section{Comparison With Multiple Core Implementation}

The application was also tested against another system that allows to distribute the workload across a network of workstations. The technology adopted is Microsoft HPC. 
This program acts as a batch queuing from a master node for a distributed computing infrastructure. The users are allowed to send jobs to the framework that will automatically distribute them across the network.
For this scenario, no data replication or means for data communication are provided by the system. Therefore, on top of this environment, only one test was chosen to be performed, because of the limited communication capabilities of the Microsoft HPC infrastructure.

The mathematical function that was selected for the comparison is the generation of the correlated random variables. This was chosen due to the perfect independence of the various runs. The matrix U needed for equation \ref{eq:crv} was distributed across all the nodes and read locally by the tasks. The results of the computations were collected on the master node.

Afterwards, all the results were collected and reduced together. For this scenario, an optimized version based on NMath \cite{nmath} for the generation of the correlated random variable was adopted.

In the following table, it is possible to see the difference among the MapReduce implementation and the one running on MS HPC.

\begin{center}
  \pgfplotstabletypeset
  [
  col sep=&,
  %    row sep=\\,
  every head row/.style={
    % as in the previous example, this patches the first row:
    before row=\hline,
    after row=\hline,
  },
  every last row/.style={after row=\hline},
  % define column-specific styles:
  columns/runs/.style={column type=|c,column name=Runs},
  columns/mshpc/.style={column type=|c,column name=MS HPC Time (s)},
  columns/hadoop/.style={column type=|c,column name=Hadoop Time (s)},
  columns/improvement/.style={column type=|c|,column name=Improvement},
  ]
  {plotdata/crv_mshpc.dat}
\end{center}

Micorsoft HPC does perform slightly better than Hadoop, in particular if a small number of runs is launched. However, the difference becomes stable if a high number of simulations is generated. If large numbers of runs are considered, Hadoop's performance is roughly 0.74 times slower than MS HPC. This scenario can be also observed in graph \ref{graph:multi-core}. 

\begin{graph}
\begin{center}
  \resizebox {\columnwidth} {!} {
    \begin{tikzpicture}
    \begin{loglogaxis}[
    legend style={ at={(0.03,0.97)},anchor=north west},
    title=Running Time,
    grid=major,
    xlabel=Num. of Runs,
    ylabel=Time (s)
    ]
    
    \addplot[red, mark=triangle*]
     table[col sep=& ,x=runs, y=mshpc] {plotdata/crv_mshpc.dat};
    \addlegendentry{MS HPC};
    
    \addplot[black, mark=square]
      table[col sep=& ,x=runs, y=hadoop]      {plotdata/crv_mshpc.dat};
    \addlegendentry{Hadoop};
    
    \end{loglogaxis}
    \end{tikzpicture}%
    \begin{tikzpicture}
    \begin{semilogxaxis}[
      legend style={ at={(0.97,0.03)},anchor=south east},
      title=Improvement,
      grid=major,
      xlabel=Num. of Runs,
      ylabel=Speedup
    ]
    
    \addplot[red, mark=triangle*]
      table[col sep=& ,x=runs, y=improvement] {plotdata/crv_mshpc.dat};    
    \addlegendentry{$\frac{MS HPC (s)}{Hadoop(s)}$};
    
    \end{semilogxaxis}
    \end{tikzpicture}  
  }
  \caption{Comparison with the multi core implementation.}
  \label{graph:multi-core}
\end{center}
\end{graph}

The simulation of MS HPC was run on a pool of 36 cores in order to match the Hadoop capabilities.

\section{Additional Experiments}
Additional scenarios were tested during the profiling phase but not developed afterwards, due to the high amount of time required for the computations and the high workload needed for performing optimizations.

First of all, several tests were performed for linear regression. Even with relatively small input sets (300MB), the computations ran in not less than 6 hours. Improvements to the code have been performed, as well. For example, the intermediate results were stored in Redis instead of the file system. However, this did not lead to improvements higher than 1.2 times. Therefore, no further experiments for this scenario were carried on.

The usage of thousands of small files was also tested. Instead of a large file containing all the information, a single file per time series was created, which gave a total of 38480 files.
For this case, a statistical function was run over the dataset. The whole computation needed more than 3 hours to complete, whereas it usually requires less than 3 minutes to finish if a single large input file is used. 
This gap can be explained by the large number of mappers started and the additional overhead in communication.
For this scenario, no further tests were performed afterwards.









