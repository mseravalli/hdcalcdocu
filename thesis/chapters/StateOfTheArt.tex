\chapter{State Of The Art} \label{chapter:StateOfTheArt}

\section{Big Data}
Big Data processing has gathered increasing interest both from  industry and from academia during the last decade. The availability, at a relatively low cost, of large storage infrastructures allowed users to generate huge amounts of raw data.  The volume of the data to be analysed quickly reached the point where a single machine was not able to handle the bulk of input in a time-responsive manner. Hence, it became necessary to find new paradigms and strategies to effectively handle the growing amount of information.

\section{MapReduce and Hadoop}
One of the pioneers that proposed a scalable and efficient solution was Google, which developed the GFS \cite{google-filesystem} in 2003 and on top of it MapReduce \cite{google-mapreduce} in 2004.  Thanks to these new technologies, it was one among the firsts to be able to effectively crunch huge amounts of data in order to build indices for its search. The advantage of the programming model they used, MapReduce, is that it provides a linear scalability with respect to the computational power provided, which represents an ideal case for the big data scenario.

In 2007, Yahoo started a free implementation of the same programming model based on Google's specification, and a year later, the project was released open source with the name of Hadoop. This framework offers a flexible and powerful solution for the industry because besides the linear scalability, it provides a fault tolerant environment that could be deployed on commodity and heterogeneous infrastructures.
In addition, other features are implemented within Hadoop (\ref{subsec:additionalFeatures} \nameref{subsec:additionalFeatures}) in order to offer more comfort and flexibility for the developers. 

After 4 years of development, the first stable version of Hadoop was released in December 2011, containing only the implementation of the distributed file system and the MapReduce framework (\ref{subsec:hadoop-1.0} \nameref{subsec:hadoop-1.0}).
However, the development of the project did not stop. Immediately, enhancements and improvements of the software were started, and only recently (15th October 2013), the newer version 2.0 was released as stable. One of the major contributions involved in this iteration is YARN: a smarter system for handling jobs, which allows to generalize the use of the framework beyond MapReduce (\ref{subsec:hadoop-2.0} \nameref{subsec:hadoop-2.0}). As an example, thanks to this system, it is possible to run MPI applications within a Hadoop cluster \cite{hadoop-mpi}.

Thanks to its flexibility and its features, Hadoop has been adopted by an increasing number of companies in order to handle massive amounts of data, and its adoption is rapidly growing \cite{hadoop-growth-ma, hadoop-growth-tmr}. A reason of its widespread could be also attributed to the fact that it is offered as a cloud computing service by important providers like Amazon \cite{amazonemr}, Google \cite{googlehadoop} and Microsoft \cite{mshdinsight}.

\subsection{Related Projects}
Another important aspect of Hadoop is its active community that is currently working on several related projects built on top of it. At the moment, some of the most widespread and used projects are HBase \cite{hbase}, Pig\cite{pig}, Hive \cite{hive-09}, Sqoop\cite{sqoop} and Flume\cite{flume}. 

HBase is a NoSQL database built on top of the Hadoop File System (HDFS), modelled after Google's BigTable \cite{google-bigtable}. One of the main aims of this database is to provide random realtime read/write access to the data. An advantage of this project lies in the possibility of accessing and modifying the contained data through the Hadoop native API and successively aggregate the data using MapReduce or other paradigms.

Pig is a high-level language providing the possibility of quickly developing programs for the analysis of large data sets. The most notable characteristic of this technology is the automatic translation to MapReduce jobs, which can be run on a Hadoop cluster.

Hive is a project initially developed by Facebook that provides data warehouse capabilities on top of the Hadoop framework. Hive allows to access and process the data with an SQL like language called HiveQL, which will be automatically translated into MapReduce jobs by the integrated query engine.

Sqoop is a utility for efficiently transferring data from structured data-stores like relational databases to Hadoop.

Flume is a distributed service that allows to stream large amounts of data to Hadoop. It also provides mechanisms for performance tuning and to recover the processes in case of failovers.

Anyway, the previous list provides only a small overview of the overall ecosystem. There are in fact a lot of projects that take advantage of Hadoop or some of its components and allow to exploit all the capabilities offered by the computational framework \cite{mahout,zookeeper,ambari,drill,accumulo,tez,giraph,hama}.

\subsection{Alternative Implementations}
After Google's paper on MapReduce \cite{google-mapreduce}, there have been several attempts to implement this specification. After an initial burst, where a lot of projects were started, only few of them have been further developed.
\begin{itemize}
	\item {\bfseries Disco}:  a lightweight framework python based started by Nokia \cite{discoMR}.
	\item {\bfseries Mincemeatpy}: another python-based MapReduce framework \cite{mincemeatpy}.
	\item {\bfseries Peregrine}: a MapReduce distribution designed to achieve high performance \cite{peregine}.
	\item {\bfseries QFS}: a faster implementation of the Hadoop File System. \cite{QFS}.
\end{itemize}

% http://www.quora.com/What-are-some-promising-open-source-alternatives-to-Hadoop-MapReduce-for-map-reduce

\section{Alternative Paradigms}
The MapReduce paradigm is only one among several solutions that have been proposed so far to address Big Data challenges. In fact, this paradigm although it provides good performance and scalability for batch jobs, it is not perfectly suitable for real time interaction. Moreover, some algorithms, when translated to this model, might suffer from major performance issues due to the presence of cycles among MapReduce jobs.

Subsequently to Google's MapReduce, several researchers worked on creating extensions to  this programming model. For example, Microsoft in 2007 proposed a new solution called Dryad \cite{microsoft-dryad, microsoft-dryadlinq} which is a framework that generalizes the Unix pipe system to offer a flexible and efficient programming framework that could be run on massive distributed systems. Similar to MapReduce, it also provides software fault tolerance and recovery.

Another extension is proposed by HaLoop \cite{haloop}. This is a modified version of Hadoop that aims to support more efficiently iterative data processing. This project tries to overcome the lack of built in support for iterative programs of MapReduce by making the task scheduler loop-aware and by adding improved caching mechanisms.

To address the real time interaction, several other technologies have been developed. In 2006, Google firstly proposed BigTable \cite{google-bigtable}, a column-based NoSQL database that allows low latency access to the data.
After that, several other implementation of the NoSQL paradigm have been developed by different companies and institutions. Notable examples are Dynamo\cite{dynamo}, Cassandra \cite{cassandra} and HBase \cite{hbase}.

However, these databases by themselves do not provide the means to aggregate and extract complex information. To overcome this, in 2010 Google developed a scalable, interactive query system called Dremel \cite{google-dremel}. Dremel allows to run aggregation queries over billion-row tables within seconds thanks to the usage of aggregator trees. Currently, there are several ongoing attempts trying to implement similar solutions based on Google's model. Some examples are Cloudera's Impala \cite{cloudera-impala}, Drill \cite{drill} and Stinger \cite{stinger} .

Other projects aiming to deliver real time analytics on Big Data are Spark \cite{spark} and Storm \cite{storm}. 
Spark is built on the idea of resilient distributed databases, i.e. an immutable collection of data with controllable persistence.
On the other hand, Storm is a complex event processor using an approach based on streams of transformations.
The two projects make use of Hadoop capabilities with respect to both the HDFS and the MapReduce frameworks.
At the moment of this writing, both Spark and Storm are still under active development and not stable enough to be employed in production environments.

Furthermore, other programming paradigms have been introduced for addressing massive parallel computations on large distributed systems. Again, one of the pioneers was Google that published a paper in 2010 about Pregel \cite{google-pregel}, a system for large-scale graph processing. The paradigm, on top of which Pregel is developed, is called Bulk Synchronous Parallel \cite{bsp}. Such a programming model allows to obtain good performance with respect to load balancing, both in terms of computation and communication. Up to now, there are two major open source implementations of Google's specification: Hama \cite{hama} and Giraph \cite{giraph}.

As previously said, Big Data is a very active area where a lot of companies and institutions are putting a lot of effort and energies. 
This can also be seen at the increasing number of paradigms and technologies that are developed to tackle the challenges of now and the next years.