\chapter{Theory} \label{chapter:Theory}

\section{The MapReduce Paradigm}
The MapReduce paradigm was first introduced by Google in 2004 \cite{google-mapreduce} because of their internal need to handle huge amounts of data for the creation of their search indexes.

This programming model was created with two main ideas in mind: scalability and fault tolerance. The paradigm is intended to be run on commodity-hardware clusters composed of thousands of nodes. For such a scenario, failures are highly probable, and they can happen in every part of the infrastructure: in disk, in the network or in the operating system. Hence, to face these issues, the paradigm was modeled in a way that the overall job can be completed, even if some tasks are unexpectedly terminated. To achieve this, every job is split in several small independent tasks that in case of failure, they can be easily rerun, on the same node or on other nodes.

The main components of the programming model are: Map, Shuffle and Reduce. Firstly, every input is associated with a key. Afterwards, the shuffle function groups the different keys together, and it distributes them across the nodes to the reducers. Lastly, during the reduction phase, an operation is performed for every single group.

The operations within the mapping and the reduction phases are the same for every input or group. In fact, every row / group can be handled independently from the others. Thanks to this, all map and reduce operations can be computed in parallel. This does not only provide scalability but also allows high flexibility when it comes to error handling and performance.

Usually, MapReduce frameworks provide efficient implementations of the Shuffle functions, and they normally offer several configuration parameters to adapt it to the specific use cases. However, it can be added manually to meet the particular needs for specialized algorithms or for performance improvements.

To better understand the paradigm, a serial implementation, using the Unix environment, can be easily proposed:
{\ttfamily
\begin{lstlisting}[language=sh]
cat input.txt | map | sort | reduce > output.txt
\end{lstlisting}
}
In this case, the map and the reduce functions are provided by the user and they both read from standard input and write to standard output. 
A further condition, for this simplified serial version, is that the reducer itself should be able to distinguish the several groups. 
This, however, is not necessary if a MapReduce framework is adopted.

When run in a distributed environment, one of the prerequisites is a distributed file system that allows to spread evenly the data across the cluster. The original technology adopted by Google is the GFS \cite{google-filesystem}, but other MapReduce implementations can use different technologies; Hadoop, for example, includes also a distributed file system called HDFS. The logical flow of the data, when run within a cluster, can be seen in figure \ref{fig:map-reduce}.
\begin{figure}[h]
  \begin{center}
    \includegraphics[width=1\textwidth]{images/map-reduce-cluster}
    \caption{Logical data flow across the three main phases of MapReduce.} \label{fig:map-reduce}
  \end{center}
\end{figure}
Data is initially spread across a distributed file system. When a job starts, the same map operation is issued on every node. The computation on the node is called a task. Afterwards, when the mapping phase is finished, the results are grouped and redistributed by the framework to be further processed. The following phase is the reduction, which is applied to every group. At the end of the process, the results are stored back in the distributed file system.

\subsection*{Advantages}
One of the most important features of Mapreduce is its scalability. Since all the mappers and reducers are independent from each other, they can be split into several tasks and executed in parallel. Thanks to this, when the amount of data to be computed surpasses the communication overhead, the framework provides almost linear scalability. 
Some limitations can be given by the network bandwidth or by some issues specific to particular MapReduce implementations. 
For example, Hadoop 1.0 contains elements, like the Job Tracker or the Name Node, that are not replicated, and in very large infrastructures, they can become a bottleneck.
However, with modified versions, it is possible to overcome this issues. There are deployments that still can efficiently make use of extremely large resource pools: Facebook, for example, reported that it was using a 100 PB Hadoop installation in mid 2012 \cite{hadoop-facebook-100pb}.

\subsection*{Drawbacks}
Although MapReduce performs very well with large amounts of data (\cite{hadoop-graysort13, hadoop-graysort09, hadoop-graysort08}), it might suffer from performance issues when not enough data is provided. This is mainly caused by the latency lags introduced by the communication overhead over the network.

Furthermore, the paradigm relies on an acyclic execution of jobs. Therefore, if an algorithm contains a series of cycles that cannot be properly converted to the MapReduce paradigm, the execution might suffer severely.

\subsection*{Performance Improvements}
Some significant improvements can be gained by tweaking the paradigm and the way the tasks are executed.

\subsubsubsection{The Combiner} 
In a distributed environment, a fourth function can be also introduced, the so called Combiner. This function acts as a reducer locally on the single nodes. It is applied after the mapping phase and before the keys are shuffled and distributed to the other nodes. The advantage is the reduced amount of communication after the mapper. Figure \ref{fig:map-combine-reduce} proposes a visualization of how the combiner acts on the result of the mapping.
\begin{figure}[h]
  \begin{center}
    \includegraphics[width=1\textwidth]{images/map-combine-reduce-cluster}
    \caption{The combiner acts as a reducer locally on the node after the mapping phase.} \label{fig:map-combine-reduce}
  \end{center}
\end{figure}
Usually, the reducer is also adopted as a combiner. Anyway, it is important to notice that such use of the reducer cannot be blindly adopted for every algorithm. For example, if the reducer need all the distinct rows produced by the mappers, the reducer itself cannot be directly used also as a combiner because it might alter the final outcome of the computation and lead to runtime errors.

\subsubsubsection{Speculative Execution} \label{par:speculative-execution}
Additional performance improvements can be obtained simply by adopting an ``eager'' execution strategy called speculative execution. One of the issues of running the framework on heterogeneous commodity clusters is that not all the hardware performs the tasks at the same speed. It might be the case that due to some misconfiguration or some outdated hardware, the overall execution slows down, simply because of a single task that takes too long. To overcome this possible problem, the tasks that are taking too much time to complete are launched on different nodes, if there are free resources available.
Eventually, the result of the task that first completes will be taken into account, while the other equivalent tasks, if still running, will be killed. This strategy is able to boost the performance of a job up to 45\% \cite{google-mapreduce}.

\subsection*{Parallel Computing Paradigms}
Since recently, parallel computing has been prerogative of research and computing centres due to the high cost of large and customized infrastructures with very efficient networking solutions. Therefore, the parallel computing models that have been developed are tailored to such system, and they mainly aim to exploit the highest performance out of the hardware. Nowadays, in the field of high performance computing, the de facto standard is the Message Passing Interface (MPI) \cite{mpi-1.0}. 
The fundamental idea of this paradigm is that the nodes perform specific operations depending on the data, and they communicate with other nodes to exchange the required information. 
The nodes are made aware of the kind of data they are handling and of their position relative to the other computing nodes. In this model, the computation within the node is tightly coupled with the data. Therefore the information has to be sent to the correct node to be processed.

In recent years, the reduction of hardware costs made large computing infrastructures affordable to private companies as well.
With clusters of non-specialized hardware, there are different necessities and priorities than in a computing center. The network, for example, represents a bottleneck since it is not able to provide extremely large bandwidths with very low latencies as in computing centers.

MapReduce perfectly fits in this context because it represents a shift of paradigm. With this model, data is always distributed across the infrastructure, and the single computing nodes are not aware of direct relations with other data or other nodes. All the operations in each phase are independent. In this case, the basic idea is that operations are sent to the data and processed locally.


\section{Hadoop Implementation} \label{sec:hadoop-implementation}
The Hadoop project consists of two main parts: the distributed file system HDFS and the MapReduce framework that runs of top of it. Hadoop is currently released at version 2. For this release, both parts underwent major changes that will be described in the following sections.

For this project, Hadoop 1 was chosen for stability reasons. The second iteration was in fact released as stable only after several months the project started.

\subsection{Hadoop 1} \label{subsec:hadoop-1.0}
The first stable version of Hadoop, was released in December 2011, and currently is employed by several large companies and institutions.

\subsubsection*{MapReduce}
Within Hadoop 1, MapReduce is implemented with a master-slave architecture. In this setting, the role of the master is undertaken by the Job Tracker. The duties of this application are to monitor the resources of the cluster and to assign the single processing tasks to the slaves (a.k.a. Task Trackers). The Task Trackers can be plugged at any time, also during execution, and once they are connected to the Job Tracker, it is possible to immediately start running new tasks on them. 
In order to know which resources are available, a heartbeat mechanism is used. 
The Job Tracker periodically checks whether all Task Trackers are alive and if they are currently idle or processing data. Moreover, in order to distribute the work more effectively, the Job Tracker keeps a list of all the nodes that are not performing properly. If a node, for example, due to a wrong configuration, systematically fails a job, it will be put in a black list and not further used for computation.

As the initial specification \cite{google-mapreduce} proposed, the Job Tracker uses speculative execution to increase performance of the jobs. For more details, see \ref{par:speculative-execution} \nameref{par:speculative-execution}.

In the typical life-cycle of a MapReduce application, the client sends a request containing a job to the Job Tracker. This checks the accessible resources and launches the tasks on the available Task Trackers in order to perform the computation on the data locally on the nodes. A representation of this process can be seen in figure \ref{fig:mapreduce-running}.
\begin{figure}[h]
  \begin{center}
    \includegraphics[width=1\textwidth]{images/job-anatomy}
    \caption{How Hadoop 1 runs a MapReduce job \cite{hadoop-definitive-guide}.} 
    \label{fig:mapreduce-running}
  \end{center}
\end{figure}

After the mapping phase, data have to be sorted and distributed to be further processed. The framework provides some embedded functions to efficiently perform this operation. As soon as the mappers start producing output, this is not directly written on disk, but it is kept in memory to start the partitioning. By default, 100MB buffers are assigned to every mapper. If the data exceed this limit, they are spilled on disk. Eventually, all the output is temporarily stored on disk, waiting to be distributed to the correct Reducer.
If Combiner functions are used, they usually help to decrease the information output from the mappers, which means that less data have to be communicated to the reducers.
The shuffle process is also shown in figure \ref{fig:mapreduce-sorting}.
\begin{figure}[h]
  \begin{center}
    \includegraphics[width=1\textwidth]{images/mapreduce-sorting}
    \caption{Shuffle in MapReduce \cite{hadoop-definitive-guide}.} 
    \label{fig:mapreduce-sorting}
  \end{center}
\end{figure}

The fault tolerance is handled by the Job Tracker. This, is in fact able to immediately restart the tasks if some nodes experience failovers. Unfortunately, in the first version, a failure of the Job Tracker becomes critical since there are no mechanisms to replicate its duties.  Moreover, the Job tracker turns out to be a bottleneck when it comes to scalability because of the overhead caused by handling all the resources and concurrent jobs within the cluster. 
In \nameref{subsec:hadoop-2.0}, this design has been however modified to allow a higher scalability and protection against failures.

\subsubsection*{HDFS}
As previously mentioned, a distributed file system is an essential prerequisite for running MapReduce on a cluster. It allows in fact to automatically distribute the data across the cluster and exploit their locality. The Hadoop File System (HDFS) allows to do this in a seamless way. 

HDFS is patterned after the Unix file system specification, but it sacrifices faithfulness to the standards to improve the performance. Even though it is not fully compliant with the standards and cannot therefore be directly mounted as a file system, it provides a set of APIs to access and modify the data, very similarly to the *nix ones.

In the first version of Hadoop, the file system is modelled in a master-slave fashion. There is a master application, called Namenode, that handles the whole distribution and replication of the information across the infrastructure. The slaves, a.k.a. Datanodes, can be hot plugged and disconnected at any moment; the Namenode will take care of redistributing the data if their current replication factor is under the specified threshold. Further duties of the Namenode include replication of the data across the cluster and the controlling if all the connected Datanodes are alive via a heartbeat mechanism.

Under this configuration, the Namenode represents a single point of failure. However, in order to increase recoverability in case of failover, a further application was added to the system: the Seconday Namenode. The purpose of this daemon is not to replace the master in case of failure but to perform periodic checkpoints of the current image of the Namenode. Hence, in case of failure, the whole system can be quickly restored to its last valid state. An overview of the architecture of HDFS can be seen in figure \ref{fig:hdfs-architecture}.
\begin{figure}[h]
  \begin{center}
    \includegraphics[width=1\textwidth]{images/hdfs-structure}
    \caption{HDFS Architecture.} 
    \label{fig:hdfs-architecture}
  \end{center}
\end{figure}

HDFS provides also a distributed cache system. This functionality allows the users to replicate a file in every single node, independently from the general replication configuration.
One advantage of this mechanism is the possibility of accessing such a file quickly from every node, without requiring additional communication.

The file system was primarily designed for storing large files, and because of this, the default block size is 64MB. However, according to the necessities of the users, this value can be manually optimized to gain further performance out of the system. One of the bottlenecks is currently presented by a large amount of small files: the metadata of the filesystem and its blocks is stored in memory. Therefore, if there are a lot of very small files, the memory footprint of the Namenode drastically increases, with the risk of exceeding the available memory.
% (http://blog.cloudera.com/blog/2009/02/the-small-files-problem/)

HDFS can be further configured to best meet the user requirements. There is the possibility of modifying the replication factor, the block size or the heartbeat interval. Additional parameters like caching policies or number of accepted failures can be further tuned to improve performance or reliability.


\subsection{Hadoop 2} \label{subsec:hadoop-2.0}
In the second version of Hadoop, some of the components underwent important changes. The aim was to decouple the services in order to further increase performance and scalability.

\subsubsection*{YARN}
In Hadoop 1, the Job Tracker was in charge of both cluster management and data processing. In the second version, the two functions are split, instead. The resource management of the cluster is now undertaken by YARN (Yet Another Resource Negotiator), and for the management of data processing, an Application Master is used for every application.

On every node, a Node Manager is running, and the set of these with the Resource Manager form the data-computation framework. The monitoring of the local resources on the nodes is performed by the Containers, which constantly check the utilization of elements such as memory, CPU, disk, network etc.

When a new job has to be run, the Application Manager is in charge of accepting the request, negotiating the resources with the Resource Manager, tracking their status and monitoring for progress. 
An overview of the architecture can be found in figure \ref{fig:yarn-architecture}.
\begin{figure}[h]
  \begin{center}
    \includegraphics[width=1\textwidth]{images/yarn-architecture-apache}
    \caption{YARN Architecture.} 
    \label{fig:yarn-architecture}
  \end{center}
\end{figure}

This new design allows a higher flexibility because a lot of different applications can make use of the Hadoop infrastructure (distributed file system with enclosed computational framework). These applications will only have to focus on handling the execution of jobs while the resource management is already provided by Hadoop itself.

\subsubsection*{MapReduce v2}
MapReduce was rewritten to be run as an application on top of Yarn. The new version provides backwards compatibility, so that existing application have only to be recompiled to be run on the newer system. 
The second version of MapReduce does not contain a Job Tracker and Task Trackers anymore, since the resource management is now handled by YARN.

\subsubsection*{HDFS Federation}
In Hadoop 2, the distributed file system was also renovated. Previously, a single namespace was provided for the whole cluster and handled by a unique Namenode. In the newer version, it is possible to create several namespaces controlled by different Namenodes. One of the advantages of this approach is that various applications, like MapReduce, HBase or Spark can run under separate namespaces (and Namenodes), creating thus less overhead.

\subsection{Additional Language Support} \label{subsec:additionalFeatures}
The Hadoop native API is Java-based. However, one of the interesting features that the framework offers is the possibility to define the MapReduce functions in other programming languages. 

There are two main interfaces that can be used: Pipes and Streaming. In both cases, during the execution, the external code is stored in the distributed cache of the file system, and it is run on top of a Task Tracker as shown in figure \ref{fig:hadoop-streaming-pipes}.

\subsubsection*{Pipes}
Pipes is a C++ library that allows to treat all the inputs and output of the MapReduce jobs as strings. The library allows to create a connection to the framework through a socket in order to read and write the values provided by MapReduce. This is particularly useful if there is the necessity to reuse existing C++ code.

\subsubsection*{Streaming}
Unlike Pipes, Streaming is a more general interface that allows to use any programming language for defining the mappers and the reducers. In this case, the idea is that Hadoop feeds the user functions via standard input and retrieve the results using the standard output.

Several frameworks have been so far developed in order to allow to program Hadoop using different languages.

% (http://blog.cloudera.com/blog/2009/05/10-mapreduce-tips/)

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=1\textwidth]{images/hadoop-streaming-pipes}
    \caption{Streaming and Pipes running on top of the Task Tracker \cite{hadoop-definitive-guide}.} 
    \label{fig:hadoop-streaming-pipes}
  \end{center}
\end{figure}