\chapter{Introduction} \label{chapter:Introduction}

\section{Big Data and Hadoop}
In the last decade the amount of information that was needed to be processed exponentially grew and it quickly reached the point where a single machine could not analyze the data in a time-responsive manner.
To overcome this problem, a lot of effort was put in the research of new programming paradigms that could tackle the challenges proposed.

One of the first models created to handle these emerging issues was proposed by Google that in 2003 developed a distributed file system  \cite{google-filesystem} and on top of it a new programming paradigm called MapReduce \cite{google-mapreduce}, which was designed to be run on top of large clusters of commodity hardware.

This paradigm gained immediate success in the research community thanks to its scalability properties, and several implementations were developed by different companies or foundations. Nowadays, the most known and widespread implementation is Hadoop. This project, originally developed by Yahoo, was subsequently released open source in 2008.
This computing framework offers a lot of features such as integrated fault tolerance and data replication among others, which make it appealing for a lot of different fields.

This technology has been adopted by an increasing number of companies in order to handle massive amounts of data, and its diffusion is rapidly growing \cite{hadoop-growth-ma, hadoop-growth-tmr}. A reason for its widespread could be also attributed to the fact that it is offered as a cloud computing service by important providers like Amazon \cite{amazonemr}, Google \cite{googlehadoop} and Microsoft \cite{mshdinsight}.

The increasing interest can be also seen in the growing community actively developing several projects based on Hadoop or on some of its components (hama, giraph, drill, hive, hbase, sqoop, pig, zoo keeper, mahout, ambari, avro, etc.) and the support provided by some major players in the IT field \cite{hadoop-devel,hadoop-sponsors}. 

Hadoop turned out to be also suitable for high performance and distributed computing. As an example of this, Hadoop has been the winner for the last years of the SortBenchmark competition, a contest where the sort capabilities of several state of the art technologies are put under stress test \cite{hadoop-graysort13, hadoop-graysort09, hadoop-graysort08}. Furthermore, it has also been applied within the scientific field \cite{hadoop-scicomp-early, hadoop-scihadoop, hadoop-bioinformatics}, and it is being used by big companies like Facebook for performing intensive real time calculation \cite{hadoop-realtime-facebook}.

\section{Project}

\subsection*{Motivation} 
As previously mentioned, Big Data is a growing field, and solutions like Hadoop are gathering interest from the financial world as well. However, only during the last year, big corporations (\cite{hadoop-morganstanley, hadoop-finance}) started to develop in-house solutions to tackle the challenge of processing large amounts of data.
These projects are not freely available to the public, and at the time of writing, there are no complete mathematical toolboxes based on MapReduce that are able to effectively deal with these challenges.
An attempt to provide statistical and machine learning functionalities is undertaken by the Mahout Project \cite{mahout}, but at the time being, it is still in an alpha version under active development, and a lot of functionalities are missing: for example, it currently does not provide any algorithm for linear regressions.

The main idea of the project is therefore to introduce the Hadoop technology within the financial environment as a high performance framework for distributed computing, in order to investigate if the MapReduce paradigm could tackle the challenges faced by this area.  
An additional point of interest is to understand whether it is possible to use it as an effective tool in the context of financial simulations of a medium size finance company and whether it provides benefits over traditional methods.

\subsection*{Overview}
In the first part of the project, it was necessary to develop a state of the art toolbox containing MapReduce based functions for performing mathematical and statical computations on financial data.

The successive step was to deploy the framework over several machines within  the computing infrastructure, composed of a network of workstations, in order to have a better understanding of how the program would behave under more realistic conditions.

Afterwards, profiling, optimizations to the code and comparisons to existing solutions were performed.

\subsection*{Technology} 
The project  is implement using a non-traditional environment for Hadoop. One of the non-functional requirements for the project is to run it natively on the Windows environment. Luckily, even though Hadoop officially supports only *nix platforms, there exist also branches of the main trunk that contain some optimizations for the Windows/Cygwin environment. Another non-functional requirement was to use the .NET platform as a development environment. To be able to address this condition, an in-house-customised version of the HDInsightSDK \cite{hdinsightsdk} was adopted.
An additional bottleneck in this paradigm is the aggregation of previously computed data. In fact, Hadoop stores the results of the intermediate operations in the distributed file system, and when these results have to be further aggregated with other data, some intensive I/O operations have to be performed; as a result, the whole computation might experience some slowdowns. 
However, this issue can be overcome in the case that the intermediate results fit into memory, by adopting a fast in-memory key-value database like Redis \cite{redis}.

\subsection*{Implementation}
The core part of the project consists of a mathematical toolbox, containing functions implemented using the MapReduce paradigm. The toolbox supports the operations that are most commonly required by the analysts at the company.  The most important functionalities implemented are: 

\begin{itemize}
  \item Statistical evaluation of the time series
  \item Outlier detection
  \begin{itemize}
    \item Statistically based
    \item Tukey outlier detection
    \item Linear regression
  \end{itemize}
  \item Correlation matrix generation
  \item Autocorrelation
  \item Correlated random variables generation
\end{itemize}

The core functions are accessible through a client server infrastructure. The configuration of a request is XML-based and allows users to set function specific parameters and optimizations for the Hadoop infrastructure.  The server manages the incoming requests by using internal queues and notifies the clients as soon as the request is completed.

\subsection*{Challenges}
During the project, challenges in several areas were faced. The algorithm implementation and successive tuning turned out to be the most time consuming parts of the project for a series of reasons:
\begin{itemize}
	\item {\bfseries The novelty of implementation}: for certain procedures, there are currently no reference implementations that could be taken as an example.
	\item {\bfseries The profiling and assessment}: the measurement presented another aspect that required particular care. It was important to provide several test cases to be able to evaluate the different behaviors of the framework.
	\item {\bfseries The tuning}: it was important to understand why certain strategies provide better results and how the Hadoop framework or the application could be configured to achieve the best possible outcomes.
\end{itemize}