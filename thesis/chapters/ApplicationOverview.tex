\chapter{Overview} \label{chapter:Overview}

\section{Requirements}
The central requirement for the project was to provide a MapReduce version of the most common functions used by the financial analysts within the company. These included mathematical and statistical computations and simplified financial simulations.

In addition, other non-functional requirements were explicitly demanded. In particular, there was the request to develop the whole project with the technologies adopted by the company, i.e. .NET platform running on a Windows environment.

Furthermore, due to security policies, no virtual machines could be adopted. Hence, the MapReduce framework had to be run natively on the Windows platform.

\section{Technologies Adopted} % and features
The corner stone technology of the project is Hadoop with its two main components: HDFS and MapReduce. However, to be able to meet the non-functional requirements, other complementary tools were employed as well: 
\begin{itemize}
  \item Cygwin to be able to use the system on a Windows platform.
  \item HDInsight to program MapReduce using Streaming with C\#.
  \item Redis to provide further performance improvements.
  \item Additional technologies to import the data from the existing database.
\end{itemize} 

\subsection*{Cygwin}
Even though its core parts are written in Java, Hadoop is officially supported only for the *nix environment. One of the main reasons is that some operations on the file system are completed using Unix tools. In particular, until Java 7, the JDK did not provide the possibility to directly create symbolic links. Therefore, in Hadoop, this operation is performed using the {\bfseries\ttfamily ln} command, which does not properly work on Windows platforms. More details on this can be found under section \ref{sec:hadoop-deployment}.
Moreover, the various components of Hadoop communicate with each other using the Secure Shell (SSH) protocol, and Cygwin is one of the few ways to install it under the Windows environment.

\subsection*{HDInsight SDK}
Only recently, Microsoft started also to include Hadoop within the Azure offer \cite{mshdinsight}, and to make it more appealing to the C\# programmers, it created a development framework for Visual Studio called HDInsight. 

The SDK provides a set of APIs very close to the original Java interface that actually allows to run jobs easily using Hadoop Streaming. 
HDInsight helps the developers by hiding the complexity of the streaming; programmers can focus only on the MapReduce functions, without the burden of reading from or writing to standard I/O.
Like the original API, Microsoft's SDK provides the basic constructs to create a MapReduce job, namely the base class for the configuration of the MapReduce framework, one for the Mapper and one CombinerReducer. 

However, at the beginning of the project, HDInsight was still under development and several important functionalities were still missing and had to be manually implemented during the project. The most important feature that was not present was the possibility of fine tuning the MapReduce framework and reading files directly from the HDFS distributed cache.

In the vanilla implementation, it was possible to set only few parameters, for the MapReduce framework, which were hard coded within HDInsight. In such a scenario, it was not possible to set the number of reducers, the number of tolerated failures before aborting the jobs, the files to be added to the distributed cache and many others.
To have access to the full range of settings provided by Hadoop, the configuration section within HDInsight had to be rewritten to allow higher flexibility.

Furthermore, to be able to read the distributed cache, small modifications had to be included in HDInsight, as well.

Eventually, an additional feature was added to the framework, namely the possibility to automatically copy the results to the local file system. This simple feature turned out to be particularly useful not only during the development phase but also for the final implementation in case the results have to be immediately visualized by a local application.

% tried to push the changes upstream, but no answer yet

\subsection*{Redis}
A small bottleneck of MapReduce relies in the aggregation of previously computed data. In fact, the results of the intermediate operations are stored in the distributed file system. When these have to be further aggregated with other data, some intensive I/O operations have to be performed; as a result, the whole computation might experience some slowdowns. 
However, this issue can be overcome in the case that the intermediate results fit into memory by adopting a fast in-memory key-value database like Redis \cite{redis}.

Redis was chosen in favor of other systems because it is currently one of the fastest key-value databases, w.r.t. latency \cite{nosql-perfomance-comparison}. Moreover, it can be easily deployed in a distributed environment since it requires minimal configuration, and it automatically creates shadow copies of the master on the slave nodes.

Furthermore, the development process also benefits from this technology in terms of reduced complexity of the code. The MapReduce functions do not have to perform complicated formatting of the input, but they can simply retrieve the correct values from the database.

%TODO: insert some reference to the profiling to show how much benefit we gain

\subsection*{Data Import}
The data were originally provided within an object-oriented database and had to be imported into HDFS.
Although there are projects, like Sqoop \cite{sqoop}, that allow to directly transfer data from existing databases to Hadoop, a more low level solution was adopted. The data were imported using some personalized scripts in Python and C\#. This strategy was adopted because of priority reasons within the project.