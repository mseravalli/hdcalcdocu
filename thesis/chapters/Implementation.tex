\chapter{Implementation} \label{chapter:Implementation}

\section{Architecture}
MapReduce is a programming paradigm that yields the maximum efficiency while running batch jobs. The JobTracker takes care of assigning the correct amount of resources to the jobs that have to be executed. It is possible that more jobs are executed at the same time, but there is no preemptive mechanism that allows some operations to overtake the current running computations.

To exploit this architecture, the application was developed to be used as an interface for the batch system.

The project is structured with a client-server architecture. 
The server acts as a broker between the client and Hadoop. It allows the clients to select and run the operations provided in the mathematical toolbox. It takes care of configuring the Hadoop environment for the job and of notifying when the results are complete. The server provides a simple first come first served queue mechanism to stream the jobs to Hadoop.

On the other hand, the client consists only of a small application that allows to submit a request to the server and receive the response when the work is finished. The client exports only few basic functionalities. This design choice was performed in order to allow to embed it in third party applications as a plug-in. For example, it can be included in Excel for computing statistics on large amounts of data; it can be embedded in Matlab, which could serve as a platform for advanced visualization, or it can be integrated in a web server, which increase the accessibility.

The client and the server communicate over the TCP/IP protocol. This allows the application to be used in a wide range of infrastructures.

The basic structure of the application and how the information is exchanged within the software can be seen in figure \ref{fig:achitecture}.
\insertfigure{images/Architecture}
             {Architecture of the application.}
             {fig:achitecture}
             {1.0}

\section{Configuration}
Every time the client sends a request for a job, both Hadoop and the mathematical operation can be configured and tuned. 

To achieve this, an XML-based configuration file was created. The operations that have to be run on the cluster usually contain several MapReduce steps. 

Every configuration file contains a ``Request'' for a complete mathematical operation, which can be composed of several MapReduce steps referred as ``Jobs''. 
A job specifies a single function from the mathematical toolbox that has to be launched by the server. All the jobs are executed in a sequential fashion; this is strictly necessary since it is often the case that successive jobs are using as input the result of a previous MapReduce step. 

For every job, it is possible to set parameters for the MapReduce framework and for the function itself. There is an XML element called ``Hadoop'' that allows to create all the parameters for the running environment. The settings that are always necessary to specify are: the input directory (``inputHDFSPath'') and the output folder (``outputHDFSPath''). Additional parameters called ``genericArgument'' can be also used to further tune Hadoop. 
An example of the configurations that can be specified are: the suggested number of mappers, the number of reducers, the maximum tolerated failures, what files to put in the distributed cache, and many more.

This last set of configurations are expressed exactly like in the Hadoop specification.
Therefore, the whole list of tunings provided by the framework can be easily used to fine tune the jobs to get the maximum performance. In case the optional configurations are not provided, default values set by Hadoop will be used.

In addition, there is an XML element called ``computations'' that allows configuring the operations from the mathematical toolbox. There are several classes of functions that can be included within this block, each having specific configuration parameters. These categories are the ones present in the mathematical toolbox (see \ref{sec:math-toolbox}).

To give more consistency to the specification of the configuration, an XML schema was created to ensure that only valid requests can be communicated.

An example of the configuration that is communicated from the client to the server can be seen in the source code proposed in Snippet \ref{snippet:xml-config}.

\begin{snippet}
  \begin{lstlisting}[ frame=single, 
  basicstyle=\ttfamily, 
  language=xml, 
  numbers=left, 
  numberstyle=\tiny\color{black},      
  caption= {}, 
  captionpos=b ]                    
    <?xml version="1.0" encoding="utf-8" ?>
    <request xmlns="http://tempuri.org/XMLSchema1.xsd">
      <job>
        <hadoop>
          <inputHDFSPath>input/release/ts</inputHDFSPath>
          <outputHDFSPath>input/release/output</outputHDFSPath>
          <genericArgument>mapred.reduce.tasks=6</genericArgument>
        </hadoop>
        <computation>
          <statistics>
            <function name="Test"/> 
          </statistics>
        </computation>
      </job>
    </request>
  \end{lstlisting}
  \caption{XML configuration example.}
  \label{snippet:xml-config}
\end{snippet}


\FloatBarrier
\section{Mathematical Toolbox} \label{sec:math-toolbox}
The mathematical toolbox provides a set of the functions that are most frequently used within the context of a financial company.

The most common type of input is a set of time series points defined by the ID of the time series, a date and the corresponding value.

As it is exposed in the following sections, there are five main areas of interest. For every function, the rationale will be explained, as well as some details about the implementation. In particular, a walkthrough for MapReduce steps will be provided. To identify the mapping of a key to a certain value, the following syntax will be used: {\ttfamily key -> value}.

\subsection*{Statistical Functions} \label{subsec:stat-func}
One of the most used set of functions for analyzing the data are basic statistics. These include mean, standard deviation, quantiles, and others.

The computation of the statistics for the time series is performed using a custom implementation, internally developed at the company. 
To achieve a high configurability, the functions are called through the reflection mechanism.
This allows flexibility both from the users' and the developers' perspective. With the same code base, new functions can be implemented in the time series library, and they can be immediately launched by users after the application is recompiled, without any further modifications.

The MapReduce steps used for performing this task are the following:

\begin{itemize}
  \item {\bfseries Map}: All time series are split in different groups.
  \begin{itemize}
    \item Input: {\ttfamily ID, Date, Value}
    \item Output: {\ttfamily ID -> Date, Value}
  \end{itemize}  
  \item {\bfseries Reduce}: For all groups, the correct statistical function is called via reflection. The values will be stored both on the distributed file system and in Redis.
  \begin{itemize}
    \item Input: {\ttfamily ID -> Date, Value}
    \item Output: {\ttfamily ID, Statistic}
  \end{itemize}  
\end{itemize}

It is possible to configure the application to perform different statistics on the time series at the same time, and for every operation, different parameters can be passed, as it can be seen in Snippet \ref{snippet:stat-config}.

\begin{snippet}
  \begin{lstlisting}[ frame=single, 
  basicstyle=\ttfamily, 
  language=xml, 
  numbers=left, 
  numberstyle=\tiny\color{black},      
  caption= {}, 
  captionpos=b ]                    
<statistics>
  <function name="Mean"/>
  <function name="StandardDeviation" />
  <function name ="Percentile">
    <parameter name="limit" type="int" value="5" />
  </function>
</statistics>
  \end{lstlisting}
  \caption{XML configuration example.}
  \label{snippet:stat-config}
\end{snippet}

\subsection*{Outliers Detection}
When talking about outliers, we do not include small variations of the data or small errors that can be caused by numerical imprecisions; we refer only to those points that are far away from other observations. 

One of the processes that is usually the source of outliers is the import process. Within this step, every day new data is acquired from different sources, normalized and then inserted into the company's database. It is often the case that during the normalization process, additional knowledge needs to be added to the time series by performing calculations or aggregating old and new information. In this context, it is crucial to provide only reliable data at the end of the process.

Unfortunately, it is very likely that during this workflow, some errors occur at one of the steps of the pipeline: the provided data might not be correct, the importing procedures contain some flaws, etc. It can also happen that the existing information, already stored in the database, is not precise: the exchange rates might be wrong, the data are imported using different formats (English number format instead of German number format), or imprecise scaling parameters are used.

The whole procedure is automatically executed. Therefore, it is not always easy to spot a posteriori where the error happened. It can also be the case that the error is not systematically generated; it could also be a temporary misconfiguration or misbehavior of a single component.

Anyway, what is vital is to be able to spot the wrong values and correct them.
Usually, the wrong points are visible by the human eye, but the course of action cannot be undertaken by a single person because of the thousands of time series that have to be examined. Hence, automatic tools need to be adopted.

It is important to notice that the outliers functions do not provide an exact solution; the outcome is a selection of probable candidates that have to be further scrutinized by an analyst.
 
The procedures that are described in the following sections provide different mechanisms to spot some of these outliers.

\subsubsection*{Outliers Implementation}
There are several functions that can be applied to detect some good outlier candidates.
The users can employ the same basic configuration file to select the preferred function. The application at runtime will detect and run the correct method.
This mechanism is implemented internally though the builder pattern. 
The essence of this pattern is to ``separate the construction of a complex object from its representation so that the same construction process can create different representations'' \cite{design-patterns-gamma, design-patterns-McLaughlin}.
The builder is able to instantiate and run at runtime the correct instance of the required subclass of OutlierFinder. Each child class overwrites the abstract method for detecting the outliers.
A small taxonomy was created to allow all the functions to be seamlessly called and to provide a structure that could be further extended in the future.

Figure \ref{fig:outliers-factory} depicts the architecture that allows to dynamically build the outliers detector. Other methods could be added by expanding the taxonomy and adding a new case in the factory.

\insertfigure{images/OutlierDiagram}
             {Outliers Class Diagram.}
             {fig:outliers-factory}
             {1.0}

However, although the basic configuration will be the same for all outliers' detection functions, specific parameters might be required for specific methods.

\FloatBarrier 
\subsubsection*{Statistically-based Methods}
The ground concept of this set of functions is to go through every time series and test it against some statistics.

There are several options available for these methods:
\begin{itemize}
  \item The values of the series are confronted with some provided statistics, and if they vary more than a certain standard deviation, they are marked as outliers.
  \item Only the mean and the standard deviation of a small window (defined by the user) is taken into account. This method is more robust if the time series contains a lot of points, or if there are several long-term fluctuations.
  \item Tukey test \label{subsubsec:Tukey}: this method is based on quartiles. The procedure proposed by Tukey sets aside an observation $Y$ if one of the following conditions is met:
  \[
    Y < \left(Q1 - 1.5IQR \right) \space \wedge \space Y > \left(Q3 + 1.5IQR \right)
  \]
  where $Q1$ is the lower quartile, $Q3$ is the upper quartile and $IQR=\left(Q3 - Q1 \right)$ denotes the interquartile range \cite{Data-Minin-Knowledge-Discovery, hoaglin2000understanding}.
    The advantage of such a function is that the quartiles are independent from the outliers, in comparison to the mean, for example.
  \item Instead of the points themselves, only the returns of the time series are taken into account.
  %TODO: define returns?? necessary??
  \begin{itemize}
    \item A point is marked as an outlier if its return is higher than a specified threshold.
    \item All the returns are calculated, and the top and bottom percentiles are marked as outliers.
  \end{itemize}
\end{itemize}

When a function is launched, the application checks at runtime whether the statistics are already present within Redis. If not, they are computed on the fly. A more accurate explanation on the steps can be found in the list below:

\begin{itemize}
  \item {\bfseries Map}: All time series are split in different groups.
  \begin{itemize}
    \item Input: {\ttfamily ID, Date, Value}
    \item Output: {\ttfamily ID -> Date, Value}
   \end{itemize} 
  \item {\bfseries Reduce}: The function specified in the configuration file will be run for every time series; the resulting outliers will be stored within the distributed file system.
  \begin{itemize}
    \item Input: {\ttfamily ID -> Date, Value}
    \item Output: {\ttfamily ID, Date, Value}
  \end{itemize}  
\end{itemize}

\subsubsection*{Linear Regression}
Another possible mechanism that could be introduced to select the outliers is curve fitting.

A series is approximated by a curve of points $y$, and the original values $t$ that have a relative error greater than a defined threshold are marked as outliers, where the relative error is defined as:
\[
  \mathbf{err} = \left|\frac{ y_i - t_i}{y_i}\right|
.
\]

The mechanism that is adopted to perform this operation is linear regression \cite{Bishop_ML}. 
For this case, we consider the linear combination of fixed nonlinear functions of $M$ input variables  $\mathbf{x}$ of the form:

\begin{align}
  y(\mathbf{x,w}) & = \underset{j=0} {\overset{M-1}{\sum}}w_{j}\phi_{j}(\mathbf{x,w}) \\
                  & = \mathbf{w^{\mathrm{T}}\phi(x)}
  \label{eq:lr}
\end{align}

where $\phi_{j}(x)$ are known as \emph{basis functions}, and $\mathbf{w^T}$ represents transpose of the weights vector. 

In this scenario, the basis functions have the form

\begin{align}
  \phi_{j}(x) = \exp\left\lbrace \frac{(x-\mu_j)^2}{2s^2} \right\rbrace \label{eq:basis-func}
\end{align}

where $\mu_j$ determines the location, and $s$ defines the spatial scale, i.e. how much influence every point has on its neighbors within the new curve.

For such a curve fitting mechanism, it is important to firstly define the vector of weights $\mathbf{w}$, which can be found by solving the following least square problem:

\begin{align}
  E(\mathbf{w}) = \frac{1}{2} \underset{i=1} {\overset{N}{\sum}}
                      \left\lbrace t_i - \mathbf{w^{\mathrm{T}}}\phi(x_i) \right\rbrace
                      \phi(x_n)^{\mathrm{T}}
  \label{eq:lr-leastsquares}
\end{align}

where $E$ defines the error from the target $t$ caused by $\mathbf{w}$.

In the application, two MapReduce implementations have been developed. The first only uses the standard components provided by Hadoop, whereas the second one leverages Redis for storing the intermediate results, speeding up the retrieval of the data and leading to a cleaner implementation.

In both cases, two subsequent phases are necessary. In the first one, the weights are computed by solving the least square problem equation \ref{eq:lr-leastsquares}. During the second step, the equation \ref{eq:lr} is applied to produce the curve and to see whether the values of the points diverge from the main trend.

One of the possible implementations in MapReduce of such an algorithm follows:

\begin{itemize}
  \item {\bfseries Map}: Divide the time series in different groups.
  \begin{itemize}
    \item Input: {\ttfamily ID, Date, Value}
    \item Output: {\ttfamily ID -> Date, Value}
  \end{itemize}  
  \item {\bfseries Reduce}: Solve equation \ref{eq:lr-leastsquares} to calculate the weights and store them either on the file system or on Redis.
  \begin{itemize}
    \item Input: {\ttfamily ID -> Date, Value}
    \item Output: {\ttfamily ID, s, weights} OR {\ttfamily void} if Redis is used
  \end{itemize}  
\end{itemize}
\begin{itemize}
  \item {\bfseries Map}: Map the time series (and their corresponding weights if these are stored in HDFS) to the correct ID 
  \begin{itemize}
    \item Input: {\ttfamily ID, Date, Value}
    \item Input: {\ttfamily ID, s, weights} if weights stored in HDFS
    \item Output: {\ttfamily ID -> Date, Value}
    \item Output: {\ttfamily ID -> s, weights} if weights stored in HDFS
  \end{itemize}  
  \item {\bfseries Reduce}: Distinguish between actual values and weight, or retrieve the weights from Redis, apply equation \ref{eq:lr} and mark as outliers the points that have a relative error higher than a given threshold.
  \begin{itemize}
    \item Input: {\ttfamily ID -> Date, Value}
    \item Input: {\ttfamily ID -> s, weights} if weights stored in HDFS
    \item Output: {\ttfamily ID, Date, Value}
  \end{itemize}  
\end{itemize}

In order to fit a curve within the time series, it is not necessary to select the whole set of points but only a subset. In the application, the users have the possibility to define the percentage of points to take into account for creating the curve, for example. 
To select the correct amount, the application will take samples at regular intervals. This parameter needs to be carefully tuned in order to be able to trade off between accuracy and speed of computation.

Furthermore, the basis function \ref{eq:basis-func} can be tuned as well by adjusting the spatial scale $s$.

Lastly, there is the possibility of defining an appropriate threshold for the error, above which the points are considered outliers.

\subsubsubsection{Optimizations} 
%
%The time series provided sometimes are defined only for the working days always and it is sometime the case that dare are missing from for some dates. This can cause imprecisions when calculating the weights of for the linear regression. A visualization of the problem can be seen in figure \ref{fig:linear-regression-we}.
%
%\insertfigure {images/OutlierDiagram}
%              {Outliers Class Diagram.}
%              {fig:linear-regression-we}
%              {1.0}
%
%The proposed solution was to map the working days to a 
%
%It could also be possible to enumerate all the days one after the other but this is not an optimal
A problem that turned out to be critical is the optimization of the mathematical computations. In particular, the least square problems presented a bottleneck in the early implementations, where a custom C\# solver was used. 
To overcome this, NMath \cite{nmath}, a high performance numerical library, was employed for the core algebra computations. This boosted the speed of the application by orders of magnitude.

\subsubsection*{Results Collector}
If not properly configured, the outcome of a single run of an outliers' detector might be misleading; if the tolerances are too low, a lot of false positives might occur.

To overcome this problem, the functions can be run multiple times on the same data but with different configuration parameters to extract more information from the time series.

Afterwards, the outcomes have to be combined with each other to spot the points that most probably lie outside of the distribution. A way to detect these values is to count the points that have the most occurrences and filter out the ones that are present only few times.

The MapReduce implementation of the algorithm is:

\begin{itemize}
  \item {\bfseries Map}: From all the different folders where the results are stored, every result is mapped to the proper ID and counted as 1 occurrence.
  \begin{itemize}
    \item Input: {\ttfamily ID, Date, Value}
    \item Output: {\ttfamily ID -> Date, Value, 1}
  \end{itemize} 
  \item {\bfseries Combine}: Multiple instances of the same point are aggregated and summed.
  \begin{itemize}
    \item Input: {\ttfamily  ID -> Date, Value, 1}
    \item Output: {\ttfamily  ID -> Date, Value, m}
  \end{itemize}  
  \item {\bfseries Reduce}: The process finishes to count all the instances and then if the points occur more times than a defined threshold, they are marked as outliers and output to the file system.
  \begin{itemize}
    \item Input: {\ttfamily  ID -> Date, Value, m}
    \item Output: {\ttfamily  ID, Date, Value}
  \end{itemize}  
\end{itemize}

In this process, it is possible to configure what outputs have to be considered and the minimum amount of occurrences to be found found to consider a point an outlier.

\subsection*{Autocorrelation}
The autocorrelation of a time series is defined as the cross-correlation of the series with itself. It is useful to identify repeating patterns. The autocorrelation $C$ of a complex function $f$ is defined as:

\begin{equation}
C(t) = \int_{-\infty}^{+\infty}\bar{f}(\tau) f(\tau - t) d\tau
\label{eq:autocorr}
\end{equation}

where $\bar{f}$ denotes the complex conjugate of $f$.

A brute force algorithm could be performed within $ O\left( n^{2}\right) $ running time \cite{autocorrelation}.

However, it is possible to calculate it with a higher computational efficiency if the problem is restated using the Wiener-Khinchin theorem \cite{wk-theorem}.

Recall that the Fourier Transform of $f(t)$ is defined by:

\begin{equation}
f(\tau) = \int_{-\infty}^{+\infty}f_{\nu}e^{-2\pi i\nu\tau}d\nu
,
\end{equation}

giving a complex conjugate of:

\begin{equation}
\bar{f}(\tau) = \int_{-\infty}^{+\infty}\bar{f}_{\nu}e^{2\pi i\nu\tau}d\nu
.
\end{equation}

Plugging $\bar{f}(\tau)$ and $f(t+\tau)$ into the autocorrelation function \ref{eq:autocorr} yields:

%\begin{equation}
\begin{align}
  C(t) & = \int_{-\infty}^{+\infty}
           \left[ \int_{-\infty}^{+\infty}\bar{f}_{\nu}e^{2\pi i\nu\tau}d\nu \right]     
           \left[ \int_{-\infty}^{+\infty}{f}_{\nu'}e^{-2\pi i\nu'(t+\tau)}d\nu' \right]
           d\tau \\
       & = \int_{-\infty}^{+\infty}\int_{-\infty}^{+\infty}\int_{-\infty}^{+\infty}
           \bar{f}_{\nu}f_{\nu'}e^{-2\pi i\tau(\nu'-\nu)}e^{-2\pi i\nu't}
           d\tau d\nu d\nu' \\
       & = \int_{-\infty}^{+\infty}\int_{-\infty}^{+\infty}
           \bar{f}_{\nu}f_{\nu'}\delta(\nu'-\nu)e^{-2\pi i\nu't}
           d\nu d\nu' \\
       & = \int_{-\infty}^{+\infty}
           \bar{f}_{\nu}f_{\nu}e^{-2\pi i\nu t}
           d\nu \\
       & = \int_{-\infty}^{+\infty}
           \left|f_{\nu}\right|^{2}e^{-2\pi i\nu t}
           d\nu \\
       & = F_{\nu}\left[\left|f_{\nu}\right|^{2}\right](t)
       .
\end{align}
%\end{equation}
Therefore, the autocorrelation can be described as the Fourier Transform of the absolute square of $f_{\nu}$ . This implementation is bounded by the running time of the FFT which is $ O\left( n \log n \right) $.

To achieve the highest performance, a low level implementation offered by Intel's Math Kernel Library \cite{mkl} was manually integrated within the C\# code.

The MapReduce steps followed to obtain a solution are:

\begin{itemize}
  \item {\bfseries Map}: All time series are split in different groups.
  \begin{itemize}
    \item Input: {\ttfamily ID, Date, Value}
    \item Output: {\ttfamily ID -> Date, Value}
  \end{itemize}  
  \item {\bfseries Reduce}: For every time series, the autocorrelation is calculated and stored within the file system.
  \begin{itemize}
    \item Input: {\ttfamily ID -> Date, Value}
    \item Output: {\ttfamily ID, Autocorrelation Vector}
  \end{itemize}  
\end{itemize}

\subsection*{Correlation Matrix} \label{subsec:corrMatrImpl}
Correlation between different time series is a widely used tool to model investment portfolios.
The correlation between two random variables $X$ and $Y$ is defined as:

\begin{equation}
  \mathrm{corr}(X,Y) = 
%    \frac{\mathrm{cov}(X,Y)}{\sigma_X \sigma_Y} =
    \frac{E\left[(X-\mu_X)(Y-\mu_Y)\right]}{\sigma_X \sigma_Y}
  ,
  \label{eq:correlation}
\end{equation}

where $\sigma$ denotes the standard deviation, $E$ represents the expected value of a random variable, and $\mu_{X}$ is the expected value of $X$ \cite{corr-coeff-wiki, corr-coeff-wolfram}. The calculation of the correlation coefficient between two time series of length $n$ was implemented following Equation \ref{eq:correlation} rewritten as:

\begin{align}
  r_{xy} = \frac{\sum x_i y_i - n\mu_X \mu_Y}
                {(n-1)\sigma_X \sigma_Y}
  ,    
\end{align}

which allows a more efficient computation of the correlation coefficient $r_{xy}$.

The correlation matrix is defined as symmetric square matrix containing all the correlation coefficients of the specified elements. 
The required amount of time and space to compute and store the matrix remains still in the order of $O(n^{2})$, where $n$ denoted the number of time series taken into account. 
This can become a bottleneck if a single process is used for the whole procedure.

For the MapReduce computation, the matrix is split in different blocks that are computed in parallel. Since the matrix is symmetric, to reduce the memory and the calculations required, all the computations can be performed only on a half of the matrix as it can be seen in figure \ref{fig:corr-0}.

\insertfigure{images/corr-0}
{Distribution of the computation of the correlation matrix.}
{fig:corr-0}
{0.5}

A more detailed explanation of the single steps follows:

\begin{itemize}
  \item {\bfseries Map}: For every input row the ID is taken.
  \begin{itemize}
    \item Input: {\ttfamily ID, Date, Value}
    \item Output: {\ttfamily 1 -> ID}
  \end{itemize}  
  \item {\bfseries Combine}: The IDs are collected and only single instances are sent to the reducer.
  \begin{itemize}
    \item Input: {\ttfamily 1 -> ID}
    \item Output: {\ttfamily 1 -> ID }
  \end{itemize}
  \item {\bfseries Reduce}: The IDs are sorted and split in $m$ sequential groups. All the IDs and their corresponding groups are stored in Redis.
  \begin{itemize}
    \item Input: {\ttfamily 1 -> ID}
    \item Output: {\ttfamily void} result stored in Redis ({\ttfamily ID -> m}).
  \end{itemize}  
\end{itemize}

A representation of the first MapReduce step is visualized in figure \ref{fig:corr-1}.

\insertfigure{images/corr-1}
{First MapReduce step for the computation of the correlation matrix \ref{fig:corr-0}.}
{fig:corr-1}
{1.0}

\begin{itemize}
  \item {\bfseries Map}: Every time series is associated with all the blocks where it has to be processed (see figure \ref{fig:corr-0}). The corresponding group of every ID is retrieved from Redis.
  \begin{itemize}
    \item Input: {\ttfamily ID, Date, Value}
    \item Output: {\ttfamily block -> group, ID, Date, Value}
  \end{itemize}  
  \item {\bfseries Reduce}: The time series are reconstructed since it is not guaranteed that their elements arrive in the correct order. The reducer calculates only the cross correlation of the elements that have a lower ID with the ones that have a higher ID. This can be performed thanks to the symmetric property of the correlation. Moreover, it allows to halve the needed computations.
  The results are stored in the file system with the following structure: first the lower ID, then the higher and eventually the correlation coefficient. With such a structure, every element of the matrix is uniquely identified within the file system.
  \begin{itemize}
    \item Input: {\ttfamily block -> group, ID, Date, Value}
    \item Output: {\ttfamily lower ID, higher ID, Value}
  \end{itemize}  
\end{itemize}

A visualization of the second step can be found in figure \ref{fig:corr-2}.

\insertfigure{images/corr-2}
{Second MapReduce step for the computation of the correlation matrix \ref{fig:corr-0}.}
{fig:corr-2}
{1.0}

The third MapReduce step is optional for the core computation of the correlation matrix. 
This stage gathers the previously computed results and creates the rows of a lower diagonal matrix.
This could be useful if the results have to be imported by another application if the required input form is the complete matrix.
\begin{itemize}
  \item {\bfseries Map}: The single values are identified by the first ID.
  \begin{itemize}
    \item Input: {\ttfamily lower ID, higher ID, Value}
    \item Output: {\ttfamily lower ID -> higher ID, Value}
  \end{itemize}  
  \item {\bfseries Reduce}: For every group, the high IDs are sorted in ascending order and the rows of the matrix are stored in the file system. The position of the row within the matrix is also added to be able to immediately identify it.
  \begin{itemize}
    \item Input: {\ttfamily ID-Major -> ID-Minor, Value}
    \item Output: {\ttfamily row position, values for the row}
  \end{itemize}  
\end{itemize}

With this computing schema, parallelism has to be traded with communication. If a lot of working units are created, one group of series has to be communicated to a lot of reducers. On the other hand, if the global set of IDs is split only in few subsets, the communication will decrease, but the amount of parallelization decreases as well. In this latter scenario, failures are more severe because the recovery time is longer.

For this application, it is possible to configure the number of groups in which the application will split the input dataset. Depending on this factor, an optimal number of reducers should be chosen as well in order to be able to assign a single block to every reducer.

\FloatBarrier 
\subsection*{Correlated Random Variables}
A very basic approach to simulate how a set of signals evolves is the generation of correlated random variables.

The computation of the correlated random variables $R_c$ requires to multiply a lot of sequences of uncorrelated normal distributed random numbers $R$ by a decomposition $U$ of the correlation matrix $C$ 

\begin{equation}
  R_c = RU
  .
  \label{eq:crv}
\end{equation}

The decomposition $U$ has to satisfy the following property:

\begin{equation}
  U^T U = C
  .
\end{equation}

In the case of this application, the final result was scaled according to the volatility $\sigma ^2$ of the single time series:
\begin{equation}
  \sigma_c ^2 = \sigma ^2 \odot R_c
  .
  \label{eq:crv-scaled}
\end{equation}

Several approaches can be adopted to compute $U$, but the majority of these are difficult to port to a parallel MapReduce implementation. 
A drawback of some parallel algorithms, like Two-Sided-Jacobi for \emph{singular value decomposition} (SVD) \cite{parallel-computing-statistics}, is that even if they can be parallelized they require several iterations over the data, which is not ideal for MapReduce.

The TSQR algorithm which is particularly suited for the MapReduce paradigm was taken in account as well \cite{tsqr-bens, tsqr-const}. Although it works well with large sparse matrices having low ranks, it cannot be used for full ranked dense matrices because in such case no parallelization would be possible. TSQR  gains in fact benefit from the fact that only few columns are considered for the computation, which yields to \emph{tall skinny matrices} that can be split into several small cases and then reduced to a single one.

At the time of writing, no existing efficient implementation of a full ranked decomposition based on MapReduce was found. Therefore, the algorithm for computing the correlated random variables was developed using a single core implementation of the SVD. A drawback for this approach is that the correlation needs to fit in memory in order to be able to compute the decomposition.

Singular value decomposition is suitable for this computation because, since $C$ is square, symmetric and positive semi-definite. The SVD produces:
 \begin{equation}
   C = V D V^*
 \end{equation}
where $D$ is a diagonal matrix, and $V*$ denotes the conjugate transpose of $V$. It follows that we can therefore write:
 \begin{equation}
   U = \sqrt{D} V^*
 .
 \end{equation}
 
The MapReduce implementation contains three steps. During the first and in the second phase, the data is loaded in Redis, and in the last step, the correlated random variables are actually computed. The first two steps can also be performed using other tools and the manually upload the values in Redis.

\begin{itemize}
  \item {\bfseries Map}: Split the time series by ID.
  \begin{itemize}
    \item Input: {\ttfamily ID, Date, Value}
    \item Output: {\ttfamily ID -> Date, Value}
  \end{itemize}  
  \item {\bfseries Reduce}: For every time series the volatility is calculated and then stored in Redis.
  \begin{itemize}
    \item Input: {\ttfamily ID -> Date, Value}
    \item Output: {\ttfamily void} the results are stored in Redis.
  \end{itemize}  
\end{itemize}

\begin{itemize}
  \item {\bfseries Map}: All necessary the values from the previously computed correlation matrix are loaded and grouped together.
  \begin{itemize}
    \item Input: {\ttfamily lower ID, higher ID, Value}
    \item Output: {\ttfamily 0 -> lower ID, higher ID, Value}
  \end{itemize}  
  \item {\bfseries Reduce}: The correlation matrix is created from the single values and from it $U$ is calculated.
  \begin{itemize}
    \item Input: {\ttfamily 0 -> lower ID, higher ID, Value}
    \item Output: {\ttfamily void} the matrix is stored in Redis.
  \end{itemize}  
\end{itemize}

The input of the mappers is the seed for the pseudo-random number generator, so that the experiment can be executed multiple times and yield always to the same result. To be able to launch \emph{k} mappers, \emph{k} files containing only a single number are created. Afterwards, the Hadoop framework will automatically launch one mapper per file.
\begin{itemize}
  \item {\bfseries Map}: For every mapper, equations \ref{eq:crv} and \ref{eq:crv-scaled} are computed $m$ times. All the results, $\sigma_c ^2$, are grouped together. 
  \begin{itemize}
    \item Input: {\ttfamily seed}
    \item Output: {\ttfamily 0 -> 1, $\sigma_c ^2$}
  \end{itemize}  
  \item {\bfseries Combine}: The $m$ runs computed by the mapper are summed together and communicated to the reducer. In this case, the combiner is absolutely necessary; without it, the whole process takes hours instead of minutes to complete, because of all the data that have to be communicated from the mappers to the reducers.
  \begin{itemize}
    \item Input: {\ttfamily  0 -> 1, $\sigma_c ^2$}
    \item Output: {\ttfamily  0 -> m, $\sigma_c ^2$}
  \end{itemize}  
  \item {\bfseries Reduce}: All the runs are summed together, and eventually, their mean (or another statistical operation) is calculated and output to HDFS.
  \begin{itemize}
    \item Input: {\ttfamily 0 -> m, $\sigma_c ^2$}
    \item Output: {\ttfamily $\overline{\sigma_c ^2}$}
  \end{itemize}  
\end{itemize}

In order to be able to launch the operation, the users have to specify the number of random variables \emph{m} that have to be generated and processed by every mapper.

For the matrix-vector multiplications and the SVD decomposition NMath, a high performance numerical library \cite{nmath} was adopted.

\section{Optimizations}
As already mentioned in the previous sections, in order to gain further performance from the code, different techniques were adopted. Where possible, C\# numerical libraries like NMath \cite{nmath} were introduced for the most computing intensive parts. 
As the library did not export the needed functionalities, low level code using the MLK \cite{mkl} was embedded in the application.
Lastly, additional improvements were obtained by creating patters to guarantee a better use of Redis. 
In particular, the Singleton Pattern \cite{design-patterns-gamma} was adopted to create a single instance of the Redis client and make use of a unique connection for every computing node. This was necessary, otherwise new connections were created for every processed record causing an overload of the Redis server and a slow down of the whole application.
